<?php use Roots\Sage\Nav; ?>

<header class="banner navbar navbar-default navbar-static-top" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="http://hackerrank.com/work">
        <image xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="https://d3keuzeb2crhkn.cloudfront.net/hackerrank/assets/brand/typemark--inverse60x200-4b269ffbe567564610e838e9d03175ec.svg" src="https://d3keuzeb2crhkn.cloudfront.net/hackerrank/assets/brand/typemark--inverse60x200-9e89ef21dc2bdc99362ed4a1512dfc01.png" width="200" height="55"></image>
      </a>
    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'walker' => new Nav\SageNavWalker(), 'menu_class' => 'nav navbar-nav navbar-right']);
      endif;
      ?>

      <ul id="menu-primary_navigation" class="nav navbar-nav navbar-right">
        <li class="menu-sample-page">
          <a href="https://www.hackerrank.com/login">Login</a>
        </li>
      </ul>
    </nav>

  </div>
</header>
