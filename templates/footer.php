<footer id="footer" class="content-info" role="contentinfo">
  <div class="container">

    <div class="row">

      <div class="col-sm-3">
        <p>
          <image xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="https://d3keuzeb2crhkn.cloudfront.net/hackerrank/assets/brand/typemark--inverse60x200-4b269ffbe567564610e838e9d03175ec.svg" src="https://d3keuzeb2crhkn.cloudfront.net/hackerrank/assets/brand/typemark--inverse60x200-9e89ef21dc2bdc99362ed4a1512dfc01.png" width="200" height="55"></image>
        </p>
        <p>
          &copy; 2015 HackerRank
        </p>
        <p>
          <a href="https://www.facebook.com/hackerrank" target="_blank">Facebook</a> -
          <a href="https://twitter.com/hackerrank" target="_blank">Twitter</a> -
          <a href="https://www.linkedin.com/company/435210?trk=prof-exp-company-name" target="_blank">LinkedIn</a>
        </p>
      </div>

      <div class="col-sm-3">
        <ul>
          <li><p class="color-alt-grey mmB"><strong>Company</strong></p></li>
          <li><a href="https://www.hackerrank.com/aboutus">About Us</a></li>
          <li><a href="https://www.hackerrank.com/careers">Careers</a></li>
          <li><a href="http://blog.hackerrank.com/">Blog</a></li>
        </ul>
      </div>

      <div class="col-sm-3">
        <ul>
          <li><p class="color-alt-grey mmB"><strong>HackerRank <i>for Work</i></strong></p></li>
          <li><a href="https://www.hackerrank.com/work/howitworks">How it Works</a></li>
          <li><a href="https://www.hackerrank.com/work/pricing">Pricing</a></li>
          <li><a href="https://www.hackerrank.com/work/codepair">CodePair</a></li>
        </ul>
      </div>

      <div class="col-sm-3">
        <ul>
          <li><p class="color-alt-grey mmB"><strong>Other</strong></p></li>
          <li><a href="https://www.hackerrank.com/work/tos" target="_blank">Terms</a></li>
          <li><a href="https://www.hackerrank.com/privacy">Privacy Policy</a></li>
          <li><a href="https://www.hackerrank.com/contactus">Contact Us</a></li>
        </ul>
      </div>

    </div>

  </div>
</footer>

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-45092266-1', 'auto');
  ga('send', 'pageview');
</script>

<!-- Google Code for leads Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 969548168;
var google_conversion_language = "en";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "7mB8CLy3klYQiMOozgM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/969548168/?label=7mB8CLy3klYQiMOozgM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- Bing Conversion Code -->
<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"4004629"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>

<script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
<script type="text/javascript">
twttr.conversion.trackPid('l53tv');</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="https://analytics.twitter.com/i/adsct?txn_id=l53tv&p_id=Twitter" />
<img height="1" width="1" style="display:none;" alt="" src="//t.co/i/adsct?txn_id=l53tv&p_id=Twitter" /></noscript>

<!-- Google Code for Remarketing Tag -->
<!-------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
 ------------------------------------------------- -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 969548168;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/969548168/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- LinkedIn -->
<noscript>script type="text/javascript">   _bizo_data_partner_id = "6802"; </script> <script type="text/javascript"> (function() {   var s = document.getElementsByTagName("script")[0];   var b = document.createElement("script");   b.type = "text/javascript";   b.async = true;   b.src = (window.location.protocol === "https:" ? "https://sjs" : "http://js") + ".bizographics.com/insight.min.js";   s.parentNode.insertBefore(b, s); })(); </script> <noscript>   <img height="1" width="1" alt="" style="display:none;" src="//www.bizographics.com/collect/?pid=6802&fmt=gif" /> </noscript>

<!-- Bizible -->
<script type="text/javascript" src="//cdn.bizible.com/scripts/bizible.js" async=""></script>
