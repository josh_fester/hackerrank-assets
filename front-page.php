<?php

  global $wp_query;
  // get resource terms
	$resource_terms = get_terms('resource_category', array('hide_empty' => 0));
	$resource_term_names = array();
	$resources = array();

	// find all resources
	$resources['all'] = new WP_Query(array(
    'posts_per_page'=>100,
    'post_type'=>'resource'
  ));
  $resource_term_names['all'] = 'All';
  $category_icons = array(
    'all' => 'th-large',
    'case-study' => 'flask',
    'e-book' => 'book',
    'guide' => 'file-text-o',
    'video' => 'youtube-play',
    'webinar' => 'desktop'
  );

  // find featured resources
  $resources_featured_large = new WP_Query(array(
    'posts_per_page'=>1,
    'post_type'=>'resource',
    'meta_query' => array(array(
      'key' => 'is_featured',
      'value' => 'large'
    ))
  ));
  $resources_featured_small = new WP_Query(array(
    'posts_per_page'=>2,
    'post_type'=>'resource',
    'meta_query' => array(array(
      'key' => 'is_featured',
      'value' => 'small'
    ))
  ));

	// find all resources in categories
	foreach($resource_terms as $term) {
		// build an array of slugs for the non-category resources
		$resource_term_names[$term->slug] = $term->name;

		$resources[$term->slug] = new WP_Query(array(
			'posts_per_page'=>60,
			'post_type'=>'resource',
			'tax_query' => array(array(
		    'taxonomy' => 'resource_category',
		    'field' => 'slug',
		    'terms' => $term->slug
			))
		));
	}

	wp_reset_postdata();

?>

<div class="archive-resource col-md-12">

<div class="row">
  <div class="col-md-8 left">
    <?php if($resources_featured_large->have_posts()): while($resources_featured_large->have_posts()): $resources_featured_large->the_post(); ?>
      <a href="<?php the_permalink(); ?>">
        <?php the_post_thumbnail(); ?>
      </a>
    <?php endwhile; endif; ?>
  </div>
  <div class="col-md-4 right">
    <div class="row">
      <?php if($resources_featured_small->have_posts()): while($resources_featured_small->have_posts()): $resources_featured_small->the_post(); ?>
        <div class="col-md-12 resource-wrap">
          <a class="resource" href="<?php the_permalink(); ?>">
            <h4><?php the_title(); ?></h4>
            <?php the_excerpt(); ?>
          </a>
        </div>
      <?php endwhile; endif; ?>
    </div>
  </div>
</div>

<hr>

<div class="row">
  <div class="col-md-12">
    <ul class="nav nav-tabs" role="tablist">
      <?php foreach($resources as $term => $resource): ?>
        <li role="presentation" class="<?php if($term=='all') echo 'active'; ?>">
          <a href="#resource-<?php echo $term; ?>" aria-controls="resource-<?php echo $term; ?>" role="tab" data-toggle="tab">
            <i class="fa fa-<?php echo $category_icons[$term]; ?>"></i><br>
            <span class="text-muted"><?php echo $resource_term_names[$term]; ?></span>
          </a>
        </li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>

<div class="row">
  <div class="tab-content">

    <?php foreach($resources as $term => $resource): ?>

      <div role="tabpanel" class="tab-pane <?php if($term=='all') echo 'active'; ?>" id="resource-<?php echo $term; ?>">

        <?php if($resource->have_posts()): while($resource->have_posts()): $resource->the_post(); ?>

          <div class="col-md-3">
            <div class="thumbnail">
              <a class="aspect-ratio-wrap" href="<?php the_permalink(); ?>">
                <div class="aspect-ratio" style="background-image:url(<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>);">
                </div>
              </a>
              <div class="caption">
                <h3>
                  <a href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
                  </a>
                </h3>
                <?php the_excerpt(); ?>
              </div>
            </div>
          </div>

        <?php endwhile; endif; ?>

      </div>

    <?php endforeach; ?>

  </div>
</div>

</div>
