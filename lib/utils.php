<?php

namespace Roots\Sage\Utils;

/**
 * Tell WordPress to use searchform.php from the templates/ directory
 */
function get_search_form() {
  $form = '';
  locate_template('/templates/searchform.php', true, false);
  return $form;
}
add_filter('get_search_form', __NAMESPACE__ . '\\get_search_form');

/**
 * Make a URL relative
 */
function root_relative_url($input) {
  preg_match('|https?://([^/]+)(/.*)|i', $input, $matches);
  if (!isset($matches[1]) || !isset($matches[2])) {
    return $input;
  } elseif (($matches[1] === $_SERVER['SERVER_NAME']) || $matches[1] === $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT']) {
    return wp_make_link_relative($input);
  } else {
    return $input;
  }
}

/**
 * Compare URL against relative URL
 */
function url_compare($url, $rel) {
  $url = trailingslashit($url);
  $rel = trailingslashit($rel);
  if ((strcasecmp($url, $rel) === 0) || root_relative_url($url) == $rel) {
    return true;
  } else {
    return false;
  }
}

/**
 * Check if element is empty
 */
function is_element_empty($element) {
  $element = trim($element);
  return !empty($element);
}

/*
  Return bootstrap carousel slides with an active one

function extract_html_slides($html) {
  $dom = new \DomDocument;
  $dom->loadHTML($html);
  $dom->preserveWhiteSpace = false;
  $divs = $dom->getElementsByTagName('div');
  $html_dict = array(
    'html' => '',
    'length' => $divs->length
  );
  $i = 0;

  foreach($divs as $div) {
    $class = $div->getAttribute('class');
    if ($i == 0) {
      $div->setAttribute('class' , 'item active');
    }
    $html_dict['html'] .= $div->c14N(false,true);
    $i++;
  }

  return $html_dict;
} */

/**
 * Return bootstrap carousel indicators
 */
function get_carousel_indicators($length, $id) {
  $html = '<ol class="carousel-indicators">';
  $html .= "<li data-target=\"#$id\" data-slide-to=\"0\" class=\"active\"></li>";

  for ($i = 1; $i < $length; $i++) {
    $html .= "<li data-target=\"#$id\" data-slide-to=\"0\"></li>";
  }

  $html .= '</ol>';

  return $html;
}

/**
 * Return bootstrap carousel controls
 */
function get_carousel_controls($id) {
  return '<a class="left carousel-control" href="#'.$id.'" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#'.$id.'" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>';
}
