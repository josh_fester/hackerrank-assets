<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Config;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Config\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * the_excerpt() length
 */
function hr_excerpt_length($length) {
	return 15;
}
add_filter('excerpt_length', __NAMESPACE__ . '\\hr_excerpt_length', 999);

/**
 * get rid of width and height attrs
 */
function hr_remove_thumbnail_dimensions( $html ) {
    $html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
    return $html;
}
add_filter('post_thumbnail_html', __NAMESPACE__ . '\\hr_remove_thumbnail_dimensions', 10);
add_filter('image_send_to_editor', __NAMESPACE__ . '\\hr_remove_thumbnail_dimensions', 10);

/**
 * Add the resource post type and taxonomy
 */
function hr_custom_post_types() {
 register_post_type('resource',
		array(
			'labels' => array(
				'name' => __('Resources'),
				'singular_name' => __('Resource'),
				'all_items' => __('All Resources'),
				'add_new' => __('Add New'),
				'add_new_item' => __('Add New Resource'),
				'edit' => __( 'Edit' ),
				'edit_item' => __('Edit Resource'),
				'new_item' => __('New Resource'),
				'view_item' => __('View Resource'),
				'search_items' => __('Search Resources'),
				'not_found' =>  __('Nothing found in the Database.'),
				'not_found_in_trash' => __('Nothing found in Trash'),
				'parent_item_colon' => ''
			),
			'description' => __( '' ),
			'public' => true,
			'publicly_queryable' => true,
			'exclude_from_search' => false,
			'show_ui' => true,
			'query_var' => true,
			'menu_position' => 8,
			'menu_icon' => 'dashicons-admin-links',
			'rewrite'	=> array( 'slug' => 'resources', 'with_front' => false ),
			'has_archive' => 'resources',
			'capability_type' => 'post',
			'hierarchical' => false,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail'),
			'map_meta_cap' => true
	 	)
	);

  // archive categories taxonomy
  register_taxonomy( 'resource_category',
  	array('resource'), /* if you change the name of register_post_type 'custom_type', then you have to change this */
  	array('hierarchical' => true,     /* if this is true, it acts like categories */
  		'labels' => array(
  			'name' => __( 'Categories' ), /* name of the custom taxonomy */
  			'singular_name' => __( 'Category' ), /* single taxonomy name */
  			'search_items' =>  __( 'Search Categories' ), /* search title for taxomony */
  			'all_items' => __( 'All Categories' ), /* all title for taxonomies */
  			'parent_item' => __( 'Parent Category' ), /* parent title for taxonomy */
  			'parent_item_colon' => __( 'Parent Category:' ), /* parent taxonomy title */
  			'edit_item' => __( 'Edit Category' ), /* edit custom taxonomy title */
  			'update_item' => __( 'Update Category' ), /* update title for taxonomy */
  			'add_new_item' => __( 'Add New Category' ), /* add new title for taxonomy */
  			'new_item_name' => __( 'New Category Name' ) /* name title for taxonomy */
  		),
  		'show_ui' => true,
  		'query_var' => true,
  		'rewrite' => array( 'slug' => 'resource-category' )
  	)
  );

  // archive categories tags
  register_taxonomy( 'resource_tag',
  	array('resource'), /* if you change the name of register_post_type 'custom_type', then you have to change this */
  	array('hierarchical' => false,     /* if this is true, it acts like categories */
  		'labels' => array(
  			'name' => __( 'Tags' ), /* name of the custom taxonomy */
  			'singular_name' => __( 'Tag' ), /* single taxonomy name */
  			'search_items' =>  __( 'Search Tags' ), /* search title for taxomony */
  			'all_items' => __( 'All Tags' ), /* all title for taxonomies */
  			'parent_item' => __( 'Parent Tag' ), /* parent title for taxonomy */
  			'parent_item_colon' => __( 'Parent Tag:' ), /* parent taxonomy title */
  			'edit_item' => __( 'Edit Tag' ), /* edit custom taxonomy title */
  			'update_item' => __( 'Update Tag' ), /* update title for taxonomy */
  			'add_new_item' => __( 'Add New Tag' ), /* add new title for taxonomy */
  			'new_item_name' => __( 'New Tag Name' ) /* name title for taxonomy */
  		),
  		'show_ui' => true,
  		'query_var' => true,
  		'rewrite' => array( 'slug' => 'resource-tag' )
  	)
  );
}
add_action('init', __NAMESPACE__ . '\\hr_custom_post_types');


/**
 * Register and login a user
 */
function hr_register_login() {
  if (isset($_POST['registerLogin'])) {

    $password = wp_generate_password();
    $credentials = array(
      'user_login' => $_POST['email'],
      'user_password' => $password,
      'remember' => true
    );
    $user_id = wp_create_user($_POST['email'], wp_generate_password(), $_POST['email']);

    if($user_id) {
      $user = get_user_by('id', $user_id);
      wp_new_user_notification($user_id, $password);

      wp_set_current_user($user_id);
      wp_set_auth_cookie($user_id);
      do_action('wp_login', $user->user_login, $user);
    }

  }
}
add_action('template_redirect', __NAMESPACE__ . '\\hr_register_login');
