<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;

?>
<?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->

    <header class="banner navbar navbar-default navbar-static-top" role="banner">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only"><?= __('Toggle navigation', 'sage'); ?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="http://hackerrank.com/work">
            <img src="<?= get_template_directory_uri(); ?>/assets/images/logo.png">
          </a>
        </div>

        <nav class="collapse navbar-collapse navbar-right" role="navigation">
          <span class="hidden-md hidden-sm hidden-xs">
            Sign up by May 31st for a chance to win<br>
            a free CodeSprint sponsorship
          </span>
          <a href="https://www.hackerrank.com/work/signup?utm_campaign=FinServ2015&utm_source=FinServLandingPage&utm_medium=FreeTrialButton" class="btn btn-default btn-white navbar-btn">Free Trial</a>
          <a href="https://www.hackerrank.com/work/request-demo?utm_campaign=FinServ2015&utm_source=FinServLandingPage&utm_medium=RequestDemoButton" class="btn btn-success navbar-btn">Request Demo</a>
        </nav>

      </div>
    </header>

    <?php include Wrapper\template_path(); ?>

    <div class="clearfix"></div>

    <?php
      get_template_part('templates/footer');
      wp_footer();
    ?>

  </body>
</html>
