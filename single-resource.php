<?php

$resource_terms = get_terms('resource_category');

?>

<?php while (have_posts()) : the_post(); ?>

  <div class="row">

    <div class="col-md-9">
      <article <?php post_class(); ?>>
        <header>
          <ol class="breadcrumb">
            <li><a href="#">Resources</a></li>
            <?php foreach($resource_terms as $term): ?>
              <li><a href="#"><?php echo $term->name; ?></a></li>
            <?php endforeach; ?>
          </ol>
          <h1 class="entry-title"><?php the_title(); ?></h1>
        </header>
        <div class="entry-content text-muted">
          <?php the_content(); ?>
        </div>
        <?php the_post_thumbnail(); ?>
      </article>
    </div>

    <div class="col-md-3">

      <div class="row social text-muted">
        <div class="col-sm-3">
          <i class="fa fa-facebook"></i>
        </div>
        <div class="col-sm-3">
          <i class="fa fa-twitter"></i>
        </div>
        <div class="col-sm-3">
          <i class="fa fa-instagram"></i>
        </div>
        <div class="col-sm-3">
          <i class="fa fa-envelope"></i>
        </div>
      </div>

      <?php if(has_term('', 'resource_tag')): ?>
        <div class="row tags">
          <div class="col-md-12">
            Topics: <?php the_terms(get_the_ID(), 'resource_tag'); ?>
          </div>
        </div>
      <?php endif; ?>

      <?php if(is_user_logged_in()): ?>

        <a href="<?php the_field('resource_asset'); ?>" class="btn btn-primary btn-lg btn-block">
          <i class="fa fa-download"></i> &nbsp;Download
        </a>

      <?php else: ?>

        <form method="post">
          <h3>Access this resource</h3>
          <p class="text-muted">Fill out the form below to access all of the resources on this site.</p>
          <div class="form-group">
            <input name="firstName" type="text" class="form-control" placeholder="First Name">
          </div>
          <div class="form-group">
            <input name="lastName" type="text" class="form-control" placeholder="Last Name">
          </div>
          <div class="form-group">
            <input name="company" type="text" class="form-control" placeholder="Company">
          </div>
          <div class="form-group">
            <input name="email" type="email" class="form-control" placeholder="Email">
          </div>
          <input type="hidden" name="registerLogin" value="true">
          <button type="submit" class="btn btn-primary btn-lg btn-block">
            Access
          </button>
        </form>

      <?php endif; ?>

    </div>

  </div>

<?php endwhile; ?>
