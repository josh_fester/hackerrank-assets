<?php
/**
 * Template Name: Financial Services
 */
?>

<?php while (have_posts()) : the_post(); ?>

  <section id="top">
    <?php if(have_rows('section-top-slides')): ?>

      <div id="top-carousel" class="carousel slide" data-ride="carousel">

        <?php echo Roots\Sage\Utils\get_carousel_indicators(count(get_field('section-top-slides')), 'top-carousel'); ?>

        <div class="carousel-inner" role="listbox">
          <?php $i=0; while(have_rows('section-top-slides')):

            the_row();
            $class = ($i==0 ? 'active ' : '');
            $class .= get_sub_field('slide_type');

          ?>

            <div class="item item-<?= $i ?> <?= $class ?>">
              <div class="overlay"></div>

              <style>
                #top-carousel .item-<?= $i ?> {
                  background-image: url(<?php the_sub_field('image'); ?>);
                }
              </style>

              <div class="carousel-caption">
                <?php the_sub_field('content'); ?>
              </div>
            </div>

          <?php $i++; endwhile; ?>
        </div>

        <?php echo Roots\Sage\Utils\get_carousel_controls('top-carousel'); ?>

      </div>

    <?php endif; ?>
  </section>

  <section id="logos">
    <?php if(have_rows('section-logos')): ?>
        <?php $i=0; while(have_rows('section-logos')): the_row(); ?>

          <img src="<?php the_sub_field('image'); ?>">

        <?php $i++; endwhile; ?>
    <?php endif; ?>
  </section>

  <div class="clearfix"></div>

  <section id="addepar">
    <div class="container text-center">
        <?php the_field('section-addepar'); ?>
    </div>
  </section>

  <div class="clearfix"></div>

  <section id="empower">
    <div class="container">
      <?php the_field('section-empower'); ?>
    </div>
  </section>

  <div class="clearfix"></div>

  <section id="trial" class="text-center">
    <h2>Improve the effectiveness of your hiring process now.</h2>
    <a class="btn btn-success btn-lg" href="https://www.hackerrank.com/work/signup?utm_campaign=FinServ2015&utm_source=FinServLandingPage&utm_medium=RequestDemoButton">Free Trial</a>
  </section>

  <div class="clearfix"></div>

<?php endwhile; ?>
